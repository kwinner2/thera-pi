package mandant;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

import environment.Path;
import sql.DatenquellenFactory;

public class Mandant {

    private IK ik;
    private String name;
    private String pathToIniPile;
    private Connection conn = null;

    public Mandant(String Ik, String name) {
        initialize(Ik);
        this.name = name;
    }

    public Mandant(String Ik) {
        initialize(Ik);
    }

    private void initialize(String Ik) {
        ik = new IK(Ik);
        if (!Ik.equals("000000000")) {
            if (conn == null) {
                try {
                    conn = new DatenquellenFactory(Ik).createConnection();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        pathToIniPile = Path.Instance.getProghome() + "ini/" + ik.digitString() + File.separator;
    }

    public String name() {
        return name;
    }

    public String ik() {
        return ik.digitString();
    }

    public IK getik() {
        return ik;
    }

    public String getIniPile() {
        return pathToIniPile;
    }

    public Connection getConnection() {
        return conn;
    }

    @Override
    public String toString() {

        return name + " - IK" + ik.digitString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(ik, name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Mandant other = (Mandant) obj;
        return Objects.equals(ik, other.ik) && Objects.equals(name, other.name);
    }

}
