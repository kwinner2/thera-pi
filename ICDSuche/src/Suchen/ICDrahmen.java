package Suchen;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JFrame;

import gui.LaF;
import logging.Logging;
import mandant.IK;
import mandant.Mandant;
import sql.DatenquellenFactory;

public class ICDrahmen implements Runnable {

    private JFrame jFrame;
    Connection conn;

    public static void main(String[] args) throws SQLException {
        LaF.setPlastic();
        new Logging("icd");
        Mandant mand;
        // IK ik;
        if (args.length > 0) {
            mand = new Mandant(args[0]);
        } else {
            mand = new Mandant("123456789");
        }
        // Connection conn = mand.getConnection();
        System.out.println(mand.getConnection()
                               .isClosed());
        ICDrahmen icd = new ICDrahmen(mand);
        icd.getJFrame();

    }

    public ICDrahmen(Mandant mand) {
        conn = mand.getConnection();

    }

    @Override
    public void run() {
        getJFrame();

    }

    public JFrame getJFrame() {
        int xWidth = 800;
        int yWidth = 600;
        int xPos = 200;
        int yPos = 200;

        jFrame = new JFrame();
        jFrame.setSize(xWidth, yWidth);
        jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jFrame.setPreferredSize(new Dimension(xWidth, yWidth));
        jFrame.setTitle("ICD-Recherche");
        jFrame.setContentPane(new ICDoberflaeche(new SqlInfo(conn)));

        Dimension screensize = java.awt.Toolkit.getDefaultToolkit()
                                               .getScreenSize();
        xPos = (int) (screensize.getWidth() / 2 - xWidth / 2 - 50);
        jFrame.setLocation(xPos, yPos);

        jFrame.setIconImage(Toolkit.getDefaultToolkit()
                                   .getImage(System.getProperty("user.dir") + File.separator + "icons" + File.separator
                                           + "mag.png")
                                   .getScaledInstance(44, 44, Image.SCALE_SMOOTH));
        jFrame.pack();
        jFrame.setVisible(true);
        return jFrame;
    }

}
